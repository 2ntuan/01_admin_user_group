<?php
	error_reporting(E_ALL & ~E_NOTICE);	
	session_start();
	
	$error 		= $success = "";	
	$action		= $_GET['action'];
	$dataSafe	= [];

	// Redirect page
	if(!in_array($action, array('add', 'edit') )){
		header('location: error.php');
		exit();
	}

	// kiểm tra token
	if(!isset($_SESSION['token'])){
		$_SESSION['token'] = "";
	}

	if($action == 'edit'){
		require_once 'includes/edit.php';
	}

	if($action == 'add'){
		require_once 'includes/addnew.php';
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<title><?php echo $titlePage;?></title>
		<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="js/my-js.js"></script>
	</head>
<body>
	<div id="wrapper">
    	<div class="title"><?php echo $titlePage;?></div>
        <div id="form">   
			<?php 
				echo $error . $success; 
				require_once 'includes/htmlForm.php';
			?>
        </div>
    </div>
</body>
</html>