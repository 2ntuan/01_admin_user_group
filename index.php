<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<title>Manage Group</title>
		<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="js/my-js.js"></script>
	</head>
	<body>
		<div id="wrapper">
			<div class="title">Manage Group</div>
			<?php				
				require_once 'connect.php';				
				session_start();

				//kiểm tra reset
				if (!isset($_SESSION['token'])) {
					$_SESSION['token'] = "";
				}

				// Xử lý POST - delete group
				include_once('includes/delete.php');
			?>
			<ul class="list">
				<?php echo $messageDelete; ?>
				<form action="" method="get" name="main-form" id="main-form">
					<?php include_once('includes/filter.php'); ?>
					<ul class="navi">
						<li class="active"><a href="index.php">Group Page</a></li>
						<li><a href="user.php">User Page</a></li>
					</ul>
					<table>
						<tr>
							<th class="cb"><input type="checkbox" name="check-all" id="check-all" /></th>
							<th class="name">Group Name</th>
							<th class="image">Image</th>
							<th>Status</th>
							<th>Default</th>
							<th>Sum</th>
							<th class="action">Action</th>
						</tr>
						<?php 
							include_once('includes/list.php');
							echo $xhtmlList; 
						?>
						<input type="hidden" value="<?php echo time(); ?>" name="token" />
					</table>
				</form>
				<div id="pagination">
					<?php echo $paginationHTML;?>
				</div>

				<div id="area-button">
					<a href="form.php?action=add">Add New</a>
					<a id="multy-delete" href="#">Delete Item</a>
				</div>
			</div>

		</div>
	</body>
</html>