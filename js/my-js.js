
$(document).ready(function(){
	$('#cancel-button').click(function(){
		if($(this).closest('form').hasClass('user')){
			window.location = $(this).attr('data-url');
		}else{
			window.location = 'index.php';
		}
		
	});
	
	$('#multy-delete').click(function(){
		$('input[name="task"]').val('delete');

		var arrChecked = $('input:checkbox:checked').map(function () {
			return this.value; 
		}).get();

		if (arrChecked.length === 0) {
			alert('Please checked item you want to delete!');
		}else{
			$('#main-form').submit();
		}

		
		
	});
	
	$('#check-all').change(function(){
		var checkStatus = this.checked;
		$('#main-form').find(':checkbox').each(function(){
			this.checked = checkStatus;
		});
	});
	
	$('.success, .notice, .error').click(function() {
		 $(this).toggle("slow");
	});

	//filter
	$(document).on("click","#filterBtn",function() {
		status 	= $("#input_status").val();
		search 	= $("#input_search").val();

		if( $('body').hasClass('tbl-user')) {
			search 	= $("#input_group").val();
		}

		if( search == '' && status == ''){
			alert("Please choose a value to filter!");
			return;
		}

		$('input[name="task"').val('filter');
		$('#main-form').submit();
	});
	
	$(document).on("click","#filterReset",function() {
		var onlyUrl = window.location.href.replace(window.location.search,'');
		window.location = onlyUrl;
	});
	
});
