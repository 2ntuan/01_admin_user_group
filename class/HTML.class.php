<?php
class HTML{
	
	static public function createSelectbox($arrData, $name, $keySelected = null, $class = null, $id = null){
		$xhtml = "";
		if(!empty($arrData)) {
			$id = ($id != null) ? $id : 'id_' . rand(1000,10000);
			$xhtml = '<select class="'.$class.'" name="'.$name.'" id="'.$id.'">';
			foreach($arrData as $key=>$value){
				if($keySelected == $key && $keySelected != null){
					$xhtml .= '<option value="'.$key.'" selected="true">'.$value.'</option>';
				}else{
					$xhtml .= '<option value="'.$key.'">'.$value.'</option>';
				}
			}
			$xhtml .= '</select>';
		}
		return $xhtml;
	}
}