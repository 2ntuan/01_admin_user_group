<?php 
require_once 'ThumbLib.inc.php';
class upload{
    // tên file
    private $_fileName;

    //kích thước size
    private $_fileSize;

    //lưu trữ đường dẫn thư mục tạm
    private $_tmp;

    // lỗi
    private $_error;

    // phần mở rộng
    private $_fileExtension;

    // file lưu trữ
    private $_fileDir;

    // hàm cấu tạo
    public function __construct($fileName){
        $file                    = $_FILES[$fileName];
        $this->_fileName         = $file['name'];
        $this->_fileSize         = $file['size'];
        $this->_tmp              = $file['tmp_name'];
        $this->_fileExtension    = $this->getExtention();
    }
    
    // lấy phần mở rộng
    public function getExtention(){
        $ext    = pathinfo($this->_fileName,PATHINFO_EXTENSION);
        return $ext;
    }

    //thiết lập phần mở rộng
    public function setExtention($arr){
       
        if(in_array(strtolower($this->_fileExtension),$arr) == false){
            $this->_error[] = "sai phần mở rộng";
        }
    }

    // thiết lập kích thước tối đa & tối thiểu
    public function setSize($min,$max){
        if($this->_fileSize > $max || $this->_fileSize  < $min){
            $this->_error[] = "sai phần kích thước";
        }
    }

    //lưu lại vào tập tin
    public function getTmp($value){
        if(file_exists($value)){
            $this->_fileDir = $value;
        }else{
            $this->_error[] ="không tồn tại tập tin lưu trữ";
        }
    }

    // kiểm tra lỗi 
    public function vail(){
        $flag = true;
        if(count($this->_error) > 0){
            $flag = false;
        }
        return $flag;
    }

    // upload
    public function upload(){
        $a = $this->_fileDir . '/' . time() . '.' . $this->_fileExtension;
        @move_uploaded_file( $this->_tmp ,$a);

        $fileName = pathinfo($a,PATHINFO_FILENAME);
        $thumb = PhpThumbFactory::create($a);
        $thumb->resize(125,125);
        $thumb->save($this->_fileDir . '/'. '125-' . $fileName . '.' . $this->_fileExtension);

        $fileName = pathinfo($a,PATHINFO_FILENAME);
        $thumb = PhpThumbFactory::create($a);
        $thumb->resize(450,450);
        $thumb->save($this->_fileDir . '/'. '450-' . $fileName . '.' . $this->_fileExtension);

    }

    // show _error
    public function showError(){
        $e ="";
       
        if(!empty($this->_error)){
            $e = '<ul class="alert alert-danger">';
            $count = is_countable($this->_error) ? count($this->_error) : null;
            for($i=0;$i < $count; $i++){
                $e .= '<li>' . $this->_error[$i] . '</li>';
            }
            $e .= '</ul>';
        }else{
            $e .= '<li class="alert alert-success"> Success </li>';
        }
        return $e;
    }

}

