-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2020 at 05:23 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpoffline`
--

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'inactive',
  `ordering` int(11) NOT NULL,
  `group_acp` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `group_default` varchar(10) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`id`, `name`, `status`, `ordering`, `group_acp`, `permission_id`, `created`, `created_by`, `modified`, `modified_by`, `image`, `group_default`) VALUES
(1, 'Marketing', 'active', 1, 0, 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'P1ly.png', 'no'),
(2, 'Member', 'inactive', 3, 0, 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'BrMs.png', 'yes'),
(8, 'Saler 02', 'active', 2, 0, 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'RFDr.png', 'no'),
(9, 'Administrator', 'active', 4, 0, 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', 'EHNL.png', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `module` varchar(100) NOT NULL,
  `controller` varchar(100) NOT NULL,
  `action` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `name`, `module`, `controller`, `action`) VALUES
(1, 'messi pesi', 'admin', 'book', 'index');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `sign` varchar(1000) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `modified` date NOT NULL,
  `modified_by` varchar(100) NOT NULL,
  `register_time` date NOT NULL,
  `register_ip` varchar(100) NOT NULL,
  `active_code` varchar(255) NOT NULL,
  `active_time` datetime DEFAULT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'inactive',
  `ordering` int(11) NOT NULL DEFAULT 12,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `firstname`, `lastname`, `password`, `avatar`, `sign`, `created`, `created_by`, `modified`, `modified_by`, `register_time`, `register_ip`, `active_code`, `active_time`, `status`, `ordering`, `group_id`) VALUES
(2, 'ronaldo', 'ronaldo@gmail.com', 'ronaldo', 'you', 'e10adc3949ba59abbe56e057f20f883e', 'SeTJ.png', 'sign 2', '2020-07-02 00:00:00', '', '0000-00-00', '', '0000-00-00', '', '', '2020-07-02 21:20:37', 'active', 0, 9),
(4, 'mario', 'mario@gmail.com', 'Mario', 'Super', 'e10adc3949ba59abbe56e057f20f883e', 'IfJr.png', '', '0000-00-00 00:00:00', '', '0000-00-00', '', '0000-00-00', '', '', '0000-00-00 00:00:00', 'inactive', 0, 9),
(10, 'johnny', 'okinawa123@gmail.com', 'Jan', 'Okinawa', 'e10adc3949ba59abbe56e057f20f883e', 'd9b1.png', '', NULL, '', '0000-00-00', '', '0000-00-00', '', '', NULL, 'inactive', 0, 1),
(11, 'tommy', 'tommy@gmail.com', 'Tommy', 'Guid', 'e10adc3949ba59abbe56e057f20f883e', 'caFd.png', '', NULL, '', '0000-00-00', '', '0000-00-00', '', '', NULL, 'inactive', 0, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
