<?php 
    require_once 'class/HTML.class.php'; 

    $arrStatus 		= array('null'=> 'Select status', 'inactive' => 'Inactive', 'active' => 'Active');
    $status			= HTML::createSelectbox($arrStatus, 'status', $dataSafe['status']);

    $arrGroupDef 	= array('null'=> 'Select group default', 'no' => 'No', 'yes' => 'Yes');
    $groupDefault	= HTML::createSelectbox($arrGroupDef, 'group_default', $dataSafe['group_default']);
?>

<form action="<?php echo $linkForm;?>" method="post" name="add-form"enctype="multipart/form-data" >
    <div class="row">
        <p>Name</p>
        <input type="text" name="name" value="<?php echo $dataSafe['name'];?>">
    </div>

    <div class="row">
        <p>Group Default</p>
        <?php echo $groupDefault;?>
    </div>
    
    <div class="row">
        <p>Status</p>
        <?php echo $status;?>
    </div>
    
    <div class="row">
        <p>Ordering</p>
        <input type="text" name="ordering" value="<?php echo $dataSafe['ordering'];?>">
    </div>

    <div class="row">
        <p>Image</p>
        <?php if($action == 'edit'){?>
            <img src="images/<?php echo $dataSafe['image'];?>" alt="Image" style="max-width: 300px;"><br /><br />
        <?php }; ?>
        <input type="file" name="file" value="<?php echo $dataSafe['image'];?>">
    </div>
    
    <div class="row">
        <input type="submit" value="Save" name="submit">
        <input type="button" value="Cancel" name="cancel" id="cancel-button">
        <input type="hidden" value="<?php echo time();?>" name="token" />
    </div>                         
</form>    
