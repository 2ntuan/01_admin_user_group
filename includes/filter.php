<div class="section-filter">
    <?php
        require_once 'class/HTML.class.php'; 
        $valSearch = isset($_REQUEST['search']) ? $_REQUEST['search'] : '' ;

        // Status
        $arrSelectStatus = [
            ''          => '--Choose a status --',
            'active'    => 'Active',
            'inactive'  => 'InActive',
        ];
        $keySelected    = isset($_REQUEST['status']) ? $_REQUEST['status'] : null;       
        $xhtmlStatus	= HTML::createSelectbox($arrSelectStatus, 'status', $keySelected, null, 'input_status'); 
    ?>

    <input type="text" name="search" value="<?php echo $valSearch; ?>" id="input_search" />
    <?php echo $xhtmlStatus; ?>
    <button type="button" id="filterBtn">Filter</button>
    <button type="button" id="filterReset">Reset</button>
    <input type="hidden" name="task" value="filter"/>
</div>