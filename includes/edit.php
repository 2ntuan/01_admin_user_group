<?php
require_once 'connect.php';
require_once 'class/Validate.class.php'; 
require_once 'class/functions.php';

$id 			= (isset($_GET['id']) && !empty($_GET['id'])) ? $_GET['id'] : '';
$titlePage		= 'EDIT GROUP';
$linkForm		= 'form.php?action=edit&id=' . $id;

$query 		    = 'SELECT name, status, ordering, image, group_default FROM `group` WHERE id = '. $id;
$dataSafe 	    = $database->singleRecord($query);		

// Redirect page
if(empty($dataSafe)) {
    header('location: error.php');
    exit();
}

$oldImage = $dataSafe['image'];
if(!empty($_POST)){
    if($_SESSION['token'] == $_POST['token']){ // refresh page
        unset($_SESSION['token']);
        header('location: ' . $linkForm);
        exit();
    }else{
        $_SESSION['token'] = $_POST['token'];
    }

    $source   = array(
        'name' 			=> $_POST['name'], 
        'status'		=> $_POST['status'], 
        'ordering'		=> $_POST['ordering'],
        'group_default'	=> $_POST['group_default']
    );
    $validate = new Validate($source);
    $validate->addRule('name', 'string',array('min' => 0 ,"max" => 10))
            ->addRule('group_default', 'yesno' , null,false)
            ->addRule('status', 'status' , null,false)
            ->addRule('ordering', 'int', array('min' => 0,"max" => 1000));
                
    if( $action == "edit" && !empty($_FILES['file']['name']) ){
        $source['image'] = $_FILES['file'];
        $validate->addRule('image', "file", array("fileObj" => $_FILES['file'] ,"entension"=>array("jpg","png"), "min" => 90,"max" => 9000000),false);
    }

    $validate->run();
    $dataSafe2 = $validate->getResult();
    $dataSafe = array_merge($dataSafe, $dataSafe2);
        
    if(!$validate->isValid()){
        $error = $validate->showErrors();
    }else{
        unset($dataSafe['file']);

        if( !empty($_FILES['file']['name']) ){					
            @unlink("images/". $oldImage );

            $imageName	= uploadImage($_FILES['file'], './images/');					
            if( !empty($imageName) ){
                $dataSafe['image'] = $imageName;
            }
        }

        $where = array(array('id', '=', $id));
        $database->update($dataSafe, $where);

        if($dataSafe['group_default'] == 'yes'){
            $data   = ['group_default' => 'no'];
            $where  = array(array('id', '!=', $id));
            $database->update($data, $where);
        }

        $success = '<div class="success">Success</div>'; 
    }

}


