<?php
require_once 'connect.php';
require_once 'class/Validate.class.php'; 
require_once 'class/functions.php';
$database->setTable('user');

$id 			= (isset($_GET['id']) && !empty($_GET['id'])) ? $_GET['id'] : '';
$titlePage		= 'EDIT USER';
$linkForm		= 'user-form.php?action=edit&id=' . $id;

$query 		    = 'SELECT username, firstname, lastname, status, avatar, email, group_id FROM `user` WHERE id = '. $id;
$dataSafe 	    = $database->singleRecord($query);		

// Redirect page
if(empty($dataSafe)) {
    header('location: error.php');
    exit();
}

$oldImage = $dataSafe['avatar'];
if(!empty($_POST)){
    if($_SESSION['token'] == $_POST['token']){ // refresh page
        unset($_SESSION['token']);
        header('location: ' . $linkForm);
        exit();
    }else{
        $_SESSION['token'] = $_POST['token'];
    }

    $source   = array(
        'username' 			=> $_POST['username'], 
        'firstname' 		=> $_POST['firstname'], 
        'lastname' 			=> $_POST['lastname'], 
        'email' 			=> $_POST['email'], 
        'password' 			=> $_POST['password'], 
        'status'		    => $_POST['status'], 
        'group_id'		    => $_POST['group_id']
    );
    $validate = new Validate($source);
    $validate->addRule('username', 'string',array('min' => 3 ,"max" => 100))
            ->addRule('firstname', 'string',array('min' => 3 ,"max" => 100))
            ->addRule('lastname', 'string',array('min' => 3 ,"max" => 100))
            ->addRule('email', 'email')
            ->addRule('password', 'string',array('min' => 6 ,"max" => 100))
            ->addRule('group_id', 'int' , array('min' => 0,"max" => 10000))   
            ->addRule('status', 'status' , null,false);
           
    if( $action == "edit" && !empty($_FILES['file']['name']) ){
        $source['avatar'] = $_FILES['file'];
        $validate->addRule('avatar', "file", array("fileObj" => $_FILES['file'] ,"entension"=>array("jpg","png"), "min" => 90,"max" => 9000000),false);
    }

    $validate->run();
    $dataSafe2 = $validate->getResult();
    $dataSafe = array_merge($dataSafe, $dataSafe2);
        
    if(!$validate->isValid()){
        $error = $validate->showErrors();
    }else{
        unset($dataSafe['file']);

        if( !empty($_FILES['file']['name']) ){					
            @unlink("images/avatar/". $oldImage );

            $imageName	= uploadImage($_FILES['file'], './images/avatar/');					
            if( !empty($imageName) ){
                $dataSafe['avatar'] = $imageName;
            }
        }

        $where = array(array('id', '=', $id));
        $dataSafe['password'] = md5($dataSafe['password']);
        $database->update($dataSafe, $where);

        $success = '<div class="success">Success</div>'; 
    }

}


