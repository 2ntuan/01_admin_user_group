<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<title>PHP FILE</title>
		<script type="text/javascript" src="../../js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="../../js/my-js.js"></script>
	</head>
	<body>
		<?php
			require_once '../../connect.php';
			$id			= $_GET['id']; 
			$query 		= "SELECT username, CONCAT(firstname ,' ', lastname) AS fullname, status, avatar FROM `user` WHERE `id` = '$id'";
			$item 		= $database->singleRecord($query);
			$dir 		= "../../images/avatar/";
			$mainurl 	= '../../user.php';
			if(!empty($item)){
				$status = ($item['status']==0) ? 'Inactive' : 'Active';
				$xhtml = '<div class="row">
								<p>ID:</p>'.$id.'
							</div>
							<div class="row">
								<p>Username:</p>'.$item['username'].'
							</div>
							<div class="row">
								<p>Fullname:</p>'.$item['fullname'] .'
							</div>
							<div class="row">
								<p>Status:</p>'.$status.'
							</div>
							<div class="row">
								<p>Avatar:</p>
								<p class="size"><img src="'.$dir.$item['avatar'].'" alt="Image" style="max-width: 150px;"></p>
							</div>
							<div class="row">
								<input type="hidden" name="id" value="'.$id.'">
								<input type="hidden" name="avatar" value="'.$item['avatar'].'">
								<input type="submit" value="Delete" name="submit">
								<input type="button" value="Cancel" name="cancel" data-url="'.$mainurl.'" id="cancel-button">
							</div>';
			}else{
				header('location: error.php');
				exit();
			}
			
			if(isset($_POST['submit'])){
				$id 	= $_POST['id'];
				$avatar 	= $_POST['avatar'];
				$query 	= "DELETE FROM `user` WHERE `id` = '$id'";
				$database->query($query);
				@unlink($dir.$avatar);
				$xhtml = '<div class="success">Success! Click vào <a href="'.$mainurl.'"> đây</a> để quay về trang quản lý.</div>';
			}
		?>
		<div id="wrapper">
			<div class="title">User Info</div>
			<div id="form">   
				<form action="" method="post" name="main-form" class="user">
					<?php echo $xhtml; ?>
				</form>    
			</div>
		</div>
	</body>
</html>
