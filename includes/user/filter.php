<div class="section-filter">
    <?php
        require_once 'class/HTML.class.php'; 

        // Group
        $query	        = "SELECT id, name, group_default FROM `group` ORDER BY name ASC";
        $arrSelectGroup	= $database->listRecord($query);
        $arrGroupDef    = [];

        $keySelected    = null;
        $arrGroupDef[''] = '--Choose a group --';
        foreach ($arrSelectGroup as $key => $value) {
            if($keySelected == null ){
                $keySelected = ($value['group_default'] == 'yes') ? $value['id'] : null;
            }
            $arrGroupDef[$value['id']] = $value['name'];
        }

        $keySelected    = isset($_REQUEST['group']) ? $_REQUEST['group'] : $keySelected;     
        $xhtmlGroup	= HTML::createSelectbox($arrGroupDef, 'group', $keySelected, null, 'input_group');

        // Status
        $arrSelectStatus = [
            ''          => '--Choose a status --',
            'active'    => 'Active',
            'inactive'  => 'InActive',
        ];
        $keySelected    = isset($_REQUEST['status']) ? $_REQUEST['status'] : null;       
        $xhtmlStatus	= HTML::createSelectbox($arrSelectStatus, 'status', $keySelected, null, 'input_status');
    ?>

    <?php echo $xhtmlGroup; ?>
    <?php echo $xhtmlStatus; ?>
    <button type="button" id="filterBtn" class="tbl-user">Filter</button>
    <button type="button" id="filterReset">Reset</button>
    <input type="hidden" name="task" value="filter"/>
</div>