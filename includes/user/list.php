<?php
require_once 'class/Pagination.class.php';

// Paging
$query 				= [];
$query[] 			= "SELECT COUNT(u.id) AS totalItems FROM `user` AS u";
$database->appendQuery($query, 'user');
$query[] 			= "ORDER BY u.ordering ASC";
$queryPaging 		= implode(" ", $query);
$totalItems			= $database->totalItem($queryPaging);
$totalItemsPerPage	= 4;
$pageRange			= 5;
$currentPage		= (isset($_GET['page'])) ? $_GET['page'] : 1;

$paginator			= new Pagination($totalItems, $totalItemsPerPage, $pageRange, $currentPage);
$paginationHTML 	= $paginator->showPagination();
$position			= ($currentPage-1)*$totalItemsPerPage;

// Data
$query 				=	[];
$query[] 			= "SELECT u.id, u.username, u.email, CONCAT(u.firstname, ' ', u.lastname) AS fullname, u.status, u.avatar, g.name AS group_name";
$query[] 			= "FROM `user` AS u LEFT JOIN `group` AS g ON u.group_id = g.id";
$database->appendQuery($query, 'user');
$query[] 			= "ORDER BY u.ordering ASC";
$query[]			= "LIMIT $position, $totalItemsPerPage";
$query				= implode(" ", $query);
$data				= $database->listRecord($query);

$xhtmlList = '';
if (!empty($data)) {
    foreach ($data as $item) {
		$id        = $item['id'];
        $image     = "images/avatar/" . $item['avatar'];
		$xhtmlList .= '<tr>
						<td class="cb"><input type="checkbox" name="checkbox[]" value="' . $id . '"></td>
						<td>' . $item['username'] . '</td>
						<td>' . $item['fullname'] . '</td>
						<td>' . $item['email'] . '</td>
						<td><img src="' . $image. '" alt="Image" style="max-width: 40px;"></td>
						<td>' . ucfirst($item['status']) . '</td>
						<td>' . $item['group_name'] . '</td>
						<td>
							<p class="action">
			                	<a href="user-form.php?action=edit&id=' . $id . '">Edit</a> |
	                			<a href="includes/user/delete-item.php?id=' . $id . '">Delete</a>
			                </p>
						</td>
			        </tr>';
    }
} else {
	if( isset( $_REQUEST['task'] ) && $_REQUEST['task'] == 'filter' ){ 
		$xhtmlList = '<tr class="nodata"><td colspan="7">Không tìm thấy được dữ liệu theo yêu cầu!</td></tr>';
	}else{
		$xhtmlList = '<tr class="nodata"><td colspan="7">Dữ liệu đang cập nhật!</td></tr>';
	}
}

