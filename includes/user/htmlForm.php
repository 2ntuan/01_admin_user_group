<?php 
    require_once 'class/HTML.class.php'; 

    // Status
    $arrStatus 		= array('null'=> 'Select status', 'inactive' => 'Inactive', 'active' => 'Active');
    $status			= HTML::createSelectbox($arrStatus, 'status', $dataSafe['status']);

    // Group
    $query = array();
    $query[] 	= "SELECT id, name, group_default";
    $query[] 	= "FROM `group`";
    //$query[] 	= "WHERE status = 'active'";
    $query[] 	= "ORDER BY name ASC";
    $query		= implode(" ", $query);
    $data		= $database->listRecord($query);

    $keySelected = null;
    foreach ($data as $key => $value) {
        if($keySelected == null ){
            $keySelected = ($value['group_default'] == 'yes') ? $value['id'] : null;
        }
        $arrGroupDef[$value['id']] = $value['name'];
    }

    if(!empty($_POST) || $dataSafe['group_id']){
        $keySelected = $dataSafe['group_id'];
    }
    $groupDefault	= HTML::createSelectbox($arrGroupDef, 'group_id', $keySelected);

    if(!empty($success) && $action == 'add'){
        $dataSafe	= [];
    }
?>

<form action="<?php echo $linkForm;?>" method="post" name="add-form"enctype="multipart/form-data" class="user">
    <div class="row">
        <p>userName</p>
        <input type="text" name="username" value="<?php echo $dataSafe['username'];?>">
    </div>

    <div class="row">
        <p>First Name</p>
        <input type="text" name="firstname" value="<?php echo $dataSafe['firstname'];?>">
    </div>

    <div class="row">
        <p>Last Name</p>
        <input type="text" name="lastname" value="<?php echo $dataSafe['lastname'];?>">
    </div>
    <div class="row">
        <p>Email</p>
        <input type="text" name="email" value="<?php echo $dataSafe['email'];?>">
    </div>

    <div class="row">
        <p>Password</p>
        <input type="password" name="password" value="">
    </div>

    <div class="row">
        <p>Group</p>
        <?php echo $groupDefault;?>
    </div>
    
    <div class="row">
        <p>Status</p>
        <?php echo $status;?>
    </div>
    
    <div class="row">
        <p>Avatar</p>
        <?php if($action == 'edit'){?>
            <img src="images/avatar/<?php echo $dataSafe['avatar'];?>" alt="Image" style="max-width: 300px;"><br /><br />
        <?php }; ?>
        <input type="file" name="file" value="<?php echo $dataSafe['avatar'];?>">
    </div>
    
    <div class="row">
        <input type="submit" value="Save" name="submit">
        <input type="button" value="Cancel" name="cancel" id="cancel-button" data-url="'../../user.php">
        <input type="hidden" value="<?php echo time();?>" name="token"/>
    </div>                         
</form>    
