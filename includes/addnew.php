<?php
require_once 'connect.php';
require_once 'class/Validate.class.php'; 
require_once 'class/functions.php';

$titlePage		= 'ADD GROUP';
$linkForm		= 'form.php?action=add';

if(!empty($_POST)){
    if($_SESSION['token'] == $_POST['token']){ // refresh page
        unset($_SESSION['token']);
        header('location: ' . $linkForm);
        exit();
    }else{
        $_SESSION['token'] = $_POST['token'];
    }

    $source   = array(
        'name' 			=> $_POST['name'], 
        'status'		=> $_POST['status'], 
        'ordering'		=> $_POST['ordering'],
        'group_default'	=> $_POST['group_default']
    );

    $source['image'] = $_FILES['file'];
    $validate = new Validate($source);
    $validate->addRule('name', 'string',array('min' => 0 ,"max" => 100))
            ->addRule('group_default', 'yesno' , null,false)
            ->addRule('status', 'status' , null,false)
            ->addRule('ordering', 'int', array('min' => 0,"max" => 1000))   
            ->addRule('image', "file", array("fileObj" => $_FILES['file'] ,"entension"=>array("jpg","png"), "min" => 90,"max" => 9000000),false);


    $validate->run();
    $dataSafe2 = $validate->getResult();
    $dataSafe = array_merge($dataSafe, $dataSafe2);

    if(!$validate->isValid()){
        $error = $validate->showErrors();
    }else{
        unset($dataSafe['file']);

        $imageName	= uploadImage($_FILES['file'], './images/');
        if( !empty($imageName) ){
            $dataSafe['image'] = $imageName;
        }
        
        $database->insert($dataSafe);

        $success = '<div class="success">Success</div>'; 
    }
			
}