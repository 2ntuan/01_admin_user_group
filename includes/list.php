<?php
require_once 'class/Pagination.class.php';

// Paging
$query 				= [];
$query[] 			= "SELECT COUNT(g.id) AS totalItems FROM `group` AS g";
$database->appendQuery($query);
$query[] 			= "ORDER BY g.ordering ASC";
$queryPaging 		= implode(" ", $query);
$totalItems			= $database->totalItem($queryPaging);
$totalItemsPerPage	= 10;
$pageRange			= 5;
$currentPage		= (isset($_GET['page'])) ? $_GET['page'] : 1;

$paginator			= new Pagination($totalItems, $totalItemsPerPage, $pageRange, $currentPage);
$paginationHTML 	= $paginator->showPagination();
$position			= ($currentPage-1)*$totalItemsPerPage;

// Data
$query 				=	[];
$query[] 			= "SELECT g.id, g.name, g.status, g.ordering, g.image, g.group_default, count(u.id) AS sum ";
$query[] 			= "FROM `group` AS g LEFT JOIN `user` AS u ON g.id = u.group_id";
$database->appendQuery($query);
$query[] 			= "GROUP BY u.group_id";
$query[] 			= "ORDER BY g.ordering ASC";
$query[]			= "LIMIT $position, $totalItemsPerPage";
$query				= implode(" ", $query);
$data				= $database->listRecord($query);

$xhtmlList = '';
if (!empty($data)) {
    foreach ($data as $item) {
		$id        = $item['id'];
        $image     = "images/" . $item['image'];
		$xhtmlList .= '<tr>
						<td class="cb"><input type="checkbox" name="checkbox[]" value="' . $id . '"></td>
						<td>' . $item['name'] . '</td>
						<td><img src="' . $image. '" alt="Image" style="max-width: 40px;"></td>
						<td>' . ucfirst($item['status']) . '</td>
						<td>' . ucfirst($item['group_default']) . '</td>
						<td>' . $item['sum'] . '</td>
						<td>
							<p class="action">
			                	<a href="form.php?action=edit&id=' . $id . '">Edit</a> |
	                			<a href="delete-item.php?id=' . $id . '">Delete</a>
			                </p>
						</td>
			        </tr>';
    }
} else {
	if( isset( $_REQUEST['task'] ) && $_REQUEST['task'] == 'filter' ){ 
		$xhtmlList = '<tr class="nodata"><td colspan="7">Không tìm thấy được dữ liệu theo yêu cầu!</td></tr>';
	}else{
		$xhtmlList = '<tr class="nodata"><td colspan="7">Dữ liệu đang cập nhật!</td></tr>';
	}
}
