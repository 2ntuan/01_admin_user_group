<?php
// MULTY DELETE
$messageDelete = '';
if (isset($_REQUEST['token'])) {
	if ($_SESSION['token'] == $_REQUEST['token']) { // refresh page
		unset($_SESSION['token']);
		header('location: ' . $_SERVER['PHP_SELF']);
		exit();
	} else {
		$_SESSION['token'] = $_REQUEST['token'];
	}
	
	// kiểm tra tồn tại của checkbox
	if (!empty($_REQUEST['checkbox'])) {
		$checkbox	= $_REQUEST['checkbox'];
	} else {
		$checkbox 	= "";
	}

	if (!empty($checkbox)) {
		$dir 	= "images/";
		foreach ($checkbox as $key => $value) {
			$qr = "SELECT `image` FROM `group` WHERE id = '" . $value . "'";
			$outValidate	= $database->singleRecord($qr);
			@unlink($dir . $outValidate['image']);
		};
		$total = $database->delete($checkbox);
		$messageDelete = '<div class="success">Có ' . $total . ' dòng được xóa!</div>';
	} else {
		if( isset( $_REQUEST['task'] ) && $_REQUEST['task'] == 'delete' ){ 
			$messageDelete = '<div class="notice">Bạn vui lòng chọn vào các dòng muốn xóa!</div>';
		}		
	}
}